# BOT Discord Cuby

Quentin BERNARD

## Description du projet

Le projet "BOT Cuby" est un BOT Discord développé en Python et hébergé sur un serveur VPS. Ce BOT a été spécialement conçu pour ajouter une fonctionnalité de tier list à un serveur Discord.

Le channel de tier list créé par BOT Cuby permet aux utilisateurs de créer des threads Discord contenant des tier lists de manière organisée, tout en permettant aux membres du serveur de discuter et d'échanger à leur sujet. Cela facilite l'organisation et la participation active des membres autour de diverses catégories de tier lists.

Vidéo de présentation : lien

## Fonctionnalités

- Commande "!cuby create tierlist URL" pour proposer une tier list en incluant l'URL de référence (par exemple, https://tiermaker.com/create/langages-de-programmation-15157336).
- Création automatique d'un fil de discussion dédié à la tier list proposée.
- Possibilité pour les utilisateurs de soumettre leurs propres tier lists dans le fil de discussion correspondant.
- Organisation structurée des tier lists, permettant une visualisation claire et une navigation aisée.
- Notifications aux utilisateurs lorsqu'une nouvelle tier list est ajoutée ou qu'un commentaire est posté.
- Fonctionnalités supplémentaires pour faciliter la modération, telles que la suppression des tiers lists non conformes.


