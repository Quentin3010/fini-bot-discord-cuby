import discord
import responses
# import discord.enums as ChannelType


async def send_thread_creation_message(message, user_message, is_private):
    try:
        response = responses.handle_response(message, user_message)
        # envoyer en mp ou dans le channel
        msg = await message.channel.send(response)

        up = '\N{THUMBS UP SIGN}'
        down = '\N{THUMBS DOWN SIGN}'
        await msg.add_reaction(up)
        await msg.add_reaction(down)
    except Exception as e:
        print(e)


def run_discord_bot():
    TOKEN = 'MTA1NDM3OTc3NDg5OTkyNTA1Mw.GUV7t7.L_Bf5UtOo2b1K0PE67T5CpwcjQk8psfpmAP3kM'
    client = discord.Client(intents=discord.Intents.all())

    @client.event
    async def on_ready():
        print(f'{client.user} is now running !')

    @client.event
    async def on_message(message):
        # le ne peut pas lire ses propres messages
        if message.author == client.user or message.author.bot:
            return

        user_message = str(message.content)
        channel = str(message.channel)

        # lis seulement les messages dans "tiers_list" commençant par un "!"
        if channel == "tier_lists":
            args = user_message.split(" ")  # !cuby create tierlist url
            # debugging
            # print(f"{username} said : '{user_message}' ({channel})")
            # print(args)
            if len(args) == 4 and args[0] == '!cuby' and args[1] == 'create' and args[2] == 'tierlist':
                try:
                    if verify_url(args[3]) == 1:
                        print("aaaaaaaaaaaaaah")
                        title = get_title(args[3])
                        await send_thread_creation_message(message, title, is_private=False)
                        thread = await message.channel.create_thread(name=title, type=discord.ChannelType.public_thread,
                                                                 auto_archive_duration=60, reason="commande cuby")
                        pin = await thread.send("Voici le lien pour faire votre propre tier list : " + args[3])
                        await pin.pin()
                except Exception as e:
                    print(e)
                    print("error while attempting to create a tierlist")
            await message.delete(delay=0)

    client.run(TOKEN)


def get_title(url):
    words = (url.split("/"))[4].split("-")
    message = words[0:len(words) - 1]
    return " ".join(message)


def verify_url(url):
    args = url.split("/")
    #print(f"{args}")
    #print(f"{len(args) == 5}")
    #print(f"{args[0]=='https:'}")
    #print(f"{len(args[1]) == 0}")
    #print(f"{args[2] == 'tiermaker.com'}")
    #print(f"{args[3] == 'create'}")
    #print(f"{len(args[4]) > 0}")
    if len(args) >= 5 and args[0] == "https:" and len(args[1]) == 0 and args[2] == "tiermaker.com" and args[3] == "create" and len(args[4]) > 0:
        return 1
    else:
        return 0

# !cuby create tierlist https://tiermaker.com/create/pokmon-starter-tier-list-2022-w--evolutions-15374159